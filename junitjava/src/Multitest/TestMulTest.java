package Multitest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMulTest {

	@BeforeAll
	static void setUpBeforeClass() {
		TestMul sub=new TestMul();
		assertEquals(2,sub.sub(10, 7, 1));
	}

	@AfterAll
	static void tearDownAfterClass() 
	{
		TestMul add=new TestMul();
		assertEquals(4,add.add1(1, 1, 1, 1));
	}

	@BeforeEach
	void setUp()
	{
		TestMul average=new TestMul();
		assertEquals(5,average.avg(6,6,3));
	}

	@AfterEach
	void test1()
	{
		TestMul division=new TestMul();
		assertEquals(5.0,division.div(10, 2));
	}

	@Test
	void test()
	{
		TestMul multipli=new TestMul();
		assertEquals(30,multipli.Multi(6, 5));
	}

}
